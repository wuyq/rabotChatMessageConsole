'use strict';

window.onload=function(){

	if(!window.WebSocket){
		alert('抱歉！你的浏览器不支持WebSocket,请使用高级浏览器打开！');
		return;
	}

	//创建一个socket实例
	var socket = new WebSocket('ws://54.222.189.154:8080');

	// 打开Socket
	socket.onopen = function(event) {

		//连接成功
		document.getElementById('chatTitle').style.display='block';

		// 监听消息
		socket.onmessage = function(event) {
			console.log('Client received a message',event);
			var msgObj=JSON.parse(event.data);
			console.log(msgObj);
			var d=new Date(msgObj.timestamp);
			var hours= d.getHours() < 10 ? '0'+ d.getHours() : d.getHours();
			var minutes= d.getMinutes() < 10 ? '0'+ d.getMinutes() : d.getMinutes();
			msgObj.timestamp=(d.getMonth()+1)+'-'+d.getDate()+' '+hours+':'+minutes;

			if(msgObj.from=='mike'){
				if(msgObj.event=='send'){
					var strHtml='<div class="msgLeftItem color1">\
									<div class="time">'+msgObj.timestamp+'</div>\
									<p><span class="content send">'+msgObj.text+'<span class="sendTip">&nbsp;&nbsp;[→]</span></span></p>\
									<div class="clear"></div>\
								</div>';
				}else{
					//return;
					var strHtml='<div class="msgRightItem color2">\
									<div class="time">'+msgObj.timestamp+'</div>\
									<p class="right-content-box">\
										<span class="content receive pull-right"><span class="receiveTip">[→]&nbsp;&nbsp;</span>'+msgObj.text+'</span>\
									</p>\
									<div class="clear"></div>\
									<div class="clear"></div>\
								</div>';
				}
				appendMessage(strHtml);
			}else if(msgObj.from=='kate'){
				if(msgObj.event=='send'){
					var strHtml='<div class="msgRightItem color1">\
									<div class="time">'+msgObj.timestamp+'</div>\
									<p class="right-content-box">\
										<span class="content send pull-right"><span class="sendTip">[←]&nbsp;&nbsp;</span>'+msgObj.text+'</span>\
									</p>\
									<div class="clear"></div>\
								</div>';
				}else{
					//return;
					var strHtml='<div class="msgLeftItem color2">\
									<div class="time">'+msgObj.timestamp+'</div>\
									<p><span class="content receive">'+msgObj.text+'<span class="receiveTip">&nbsp;&nbsp;[←]</span></span></p>\
									<div class="clear"></div>\
								</div>';
				}
				appendMessage(strHtml);
			}
		};

		// 监听Socket的关闭
		socket.onclose = function(event) {
			console.log('Client notified socket has closed',event);
			appendMessage('<span style="color: red;font-size: 14px;">Client notified socket has closed</span>');
		};

		// 关闭Socket....
		//socket.close()
	};

	socket.onerror=function(event){
		console.log('onerror',event.data);
		appendMessage('<span style="color: red;font-size: 14px;">网络出错...</span>')
		setTimeout(function(){
			socket=new WebSocket('ws://54.222.189.154:8080');
		},3000)
	}

	function appendMessage(strMsg){
		var oChatMsgContainer=document.getElementById('msgListContainer');
		var oMsg=document.createElement('p');
		oMsg.className='message';
		oMsg.innerHTML=strMsg;
		oChatMsgContainer.appendChild(oMsg);
		var clientHeight=document.documentElement.clientHeight;
		var scrollTop=oChatMsgContainer.scrollHeight-(clientHeight-100);
		document.body.scrollTop=scrollTop;
	}
}

